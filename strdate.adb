--  strdate - format date strings (my first little Ada experiment)
--
--  Copyright (C) 2012 Benjamin Althues
--
--  This software is provided 'as-is', without any express or implied
--  warranty.  In no event will the authors be held liable for any damages
--  arising from the use of this software.
--
--  Permission is granted to anyone to use this software for any purpose,
--  including commercial applications, and to alter it and redistribute it
--  freely, subject to the following restrictions:
--
--  1. The origin of this software must not be misrepresented; you must not
--     claim that you wrote the original software. If you use this software
--     in a product, an acknowledgment in the product documentation would be
--     appreciated but is not required.
--  2. Altered source versions must be plainly marked as such, and must not be
--     misrepresented as being the original software.
--  3. This notice may not be removed or altered from any source distribution.

with Ada.Command_Line;
with Ada.Integer_Text_IO;
with Ada.IO_Exceptions;
with Ada.Text_IO;
use Ada;

procedure strdate is
    args      : Boolean := False;
    arg_count : constant Natural := Command_Line.Argument_Count;
    day       : Integer;
    month     : Integer;
    year      : Integer;

    function GetDayAbbr return String is
    begin
        case day is
            when 1 | 21 | 31 => return "st";
            when 2 | 22 => return "nd";
            when 3 | 23 => return "rd";
            when 4 .. 20 | 24 .. 30 => return "th";
            when others => return "";
        end case;
    end GetDayAbbr;

    function GetMonthName return String is
    begin
        case month is
            when 1 => return "January";
            when 2 => return "February";
            when 3 => return "March";
            when 4 => return "April";
            when 5 => return "May";
            when 6 => return "June";
            when 7 => return "July";
            when 8 => return "August";
            when 9 => return "September";
            when 10 => return "October";
            when 11 => return "November";
            when 12 => return "December";
            when others => return "";
        end case;
    end GetMonthName;

    function GetDayOfWeek return String is
        m    : Integer;
        y    : Integer;
        cent : Integer;
    begin
        if month < 3 then
            y := year - 1;
            m := month + 10;
        else
            m := month - 2;
        end if;

        cent := y / 100;
        y := y mod 100;

        case (((26*m - 2)/10 + day + y + y/4 + cent/4 - 2*cent) mod 7) is
            when 0 => return "Monday";
            when 1 => return "Tuesday";
            when 2 => return "Wednesday";
            when 3 => return "Thursday";
            when 4 => return "Friday";
            when 5 => return "Saturday";
            when 6 => return "Sunday";
            when others => return "";
        end case;
    end GetDayOfWeek;

    procedure Warn(t : String) is
    begin
        Text_IO.Put_Line("Invalid " & t & ", please try again");
    end Warn;

    procedure Get_Date_From_Input is
    begin
        loop
            begin
                Text_IO.Put("Enter year  [YYYY]: ");
                Integer_Text_IO.Get(year);
                if year < -9999 or else year > 9999 then
                    Warn("year");
                else
                    exit;
                end if;
            exception
                when IO_EXCEPTIONS.DATA_ERROR =>
                    Warn("year");
                    Text_IO.Skip_Line;
            end;
        end loop;
        loop
            begin
                Text_IO.Put("Enter month   [MM]: ");
                Integer_Text_IO.Get(month);
                if month < 1 or else month > 12 then
                    Warn("month");
                else
                    exit;
                end if;
            exception
                when IO_EXCEPTIONS.DATA_ERROR =>
                    Warn("month");
                    Text_IO.Skip_Line;
            end;
        end loop;
        loop
            begin
                Text_IO.Put("Enter day     [DD]: ");
                Integer_Text_IO.Get(day);
                if day < 1 or else day > 31 then
                    Warn("day");
                else
                    exit;
                end if;
            exception
                when IO_EXCEPTIONS.DATA_ERROR =>
                    Warn("day");
                    Text_IO.Skip_Line;
            end;
        end loop;
    end Get_Date_From_Input;

    procedure Interactive is
    begin
        Get_Date_From_Input;
        Text_IO.New_Line;
        Text_IO.Put_Line("You entered: " & GetDayOfWeek & " " & GetMonthName &
                         day'img & GetDayAbbr & year'img);
    end;

    procedure Usage is
    begin
        Text_IO.Put_Line("strdate - format date strings");
        Text_IO.New_Line;
        Text_IO.Put_Line("-h, --help            show this help message");
        Text_IO.Put_Line("-i, --interactive     start interactive mode");
    end;
begin
    for i in 1..arg_count loop
        args := True;
        if Command_Line.Argument(i) = "-i" or
                Command_Line.Argument(i) = "--interactive" then
            Interactive;
            exit;
        else
            Usage;
            exit;
        end if;
    end loop;

    if not args then
        Usage;
    end if;
end strdate;
